import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { SQLite } from '@ionic-native/sqlite';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Base64 } from '@ionic-native/base64';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Network } from '@ionic-native/network';
import { FileOpener } from '@ionic-native/file-opener'
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { DetailSolicitationPage } from '../pages/detail-solicitation/detail-solicitation';

import { AuthService } from '../providers/auth.service';
import { PreferencesService } from '../providers/preferences.service';
import { AddSolicitationPage } from '../pages/add-solicitation/add-solicitation';
import { PopoverPage } from '../pages/popover/popover';
import { SqliteHelperService } from '../providers/sqlite-helper.service';
import { CreateServiceService } from '../providers/create-service.service';
import { ServiceSubService } from '../providers/service-sub.service';
import { SolicitationService } from '../providers/solicitation.service';
import { CameraService } from '../providers/camera.service';
import { ProfilePage } from '../pages/profile/profile';
import { ModalContentPage } from '../pages/modal-content/modal-content';
import { ModalCommentPage } from '../pages/modal-comment/modal-comment';
import { SafeHtml } from '../pipes/safe_html';
import { ComponentsModule } from '../components/components.module';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BackgroundMode } from '@ionic-native/background-mode';
import { ForgotpasswordPage } from '../pages/forgotpassword/forgotpassword';
import { ModalAttributePage } from '../pages/modal-attribute/modal-attribute';
import { PopoverOptionsDetailSolicitationPage } from '../pages/popover-options-detail-solicitation/popover-options-detail-solicitation';
import { CurrentSolicitationService } from '../providers/current_solicitation.service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    AddSolicitationPage,
    DetailSolicitationPage,
    PopoverPage,
    ProfilePage,
    ModalContentPage,
    ModalCommentPage,
    SafeHtml,
    ForgotpasswordPage,
    ModalAttributePage,
    PopoverOptionsDetailSolicitationPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),    
    IonicStorageModule.forRoot({
      name: '__casprodaterlite',
      storeName: 'user',
      driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
    }),
    ComponentsModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SigninPage,
    AddSolicitationPage,
    DetailSolicitationPage,
    PopoverPage,
    ProfilePage,
    ModalContentPage,
    ModalCommentPage,
    ForgotpasswordPage,
    ModalAttributePage,
    PopoverOptionsDetailSolicitationPage
  ],
  providers: [
    FileOpener,
    Network,
    File,
    FilePath,
    Base64,
    Camera,
    SQLite,
    StatusBar,
    SplashScreen,
    PhotoViewer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthService,
    PreferencesService,
    SqliteHelperService,
    CreateServiceService,
    ServiceSubService,
    SolicitationService,
    CameraService,
    InAppBrowser,
    BackgroundMode,
    CurrentSolicitationService
  ]
})
export class AppModule {}
