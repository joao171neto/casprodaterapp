import { Component } from '@angular/core';
import { Platform, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';

import { SigninPage } from '../pages/signin/signin';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = SigninPage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    private toastCtrl: ToastController,
    private network: Network 
  ) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#002254');
      this.hideSplashscreen();
      this.onDisconnect("Não Há conexão com a Internet!")
    });
  }
 
  private hideSplashscreen(): void {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 500);
    }
  }

  private onDisconnect(message: string){
    this.network.onDisconnect().subscribe(data => {
      this.toastCtrl.create({
        message: message,
        duration: 3000
      }).present()
    }, error => console.log(error))
  }
}

