export class Solicitation{
    public id: string;

    public approver_email: string;
    public approver_id: string;
    public caller_id: string;
    public caller_id_friendlyname: string;
    public caller_name: string;
    public close_date: string;
    public description: string;
    public end_date: string;
    public last_update: string;
    public org_id: string;
    public org_name: string;
    public origin: string;
    public ref: string;
    public request_type: string;
    public service_id: string;
    public service_name: string;
    public servicesubcategory_id: string;
    public servicesubcategory_name: string;
    public start_date: string;
    public status: string;
    public title: string;

    constructor(){}
}