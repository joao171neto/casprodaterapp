export class Team {
    public id?: number;
    public _id: string;
    public name: string;
    public status: string;
    public org_id: string;
    public org_name: string;
    public phone: string;
    public notify: string;
    public function: string;
    public friendlyname: string;
    public org_id_friendlyname: string;

    constructor(
        
    ){}
}