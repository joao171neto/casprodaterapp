import { Component, Input } from '@angular/core';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
import $ from "jquery";

import { SafeHtml } from './../../pipes/safe_html';
import { Solicitation } from '../../models/solicitation.model';
import { LoadingController, Loading, ToastController } from 'ionic-angular';
import { PreferencesService } from '../../providers/preferences.service';
import { Attchatment } from '../../models/attachment.model';
import { User } from '../../models/user.model';

@Component({
  selector: 'attachments',
  templateUrl: 'attachments.html'
})
export class AttachmentsComponent {

  @Input() solicitation:Solicitation;
  loading: Loading;
  attachments: Attchatment[] = [];
  user: User;
  aplicationType: string;
  safeHtml: SafeHtml;

  constructor(
    private loadingCtrl: LoadingController,
    private preferences: PreferencesService,
    private file: File,
    private fileOpener: FileOpener,
    public toastCtrl: ToastController
  ) {

    this.loading = this.showLoading();
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getAttachments({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        })
      })
  }
  
  getAttachments(user: { username: string, password: string, id: string, org_id: string }){
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "Attachment",
        "output_fields": "*",
        "key": "SELECT Attachment WHERE item_id LIKE '%"+ this.solicitation.id +"'"
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
          for (var i in data['objects']) {
            let name = data['objects'][i].fields.contents.filename;
            let attachment: Attchatment = {
              id: data['objects'][i].key,
              image: data['objects'][i].fields.contents.data,
              filename: name,
              mimeType: name.slice((name.length - 4), name.length)
            }
            self.attachments.unshift(attachment);
          }

          self.loading.dismiss();
        }
    });
  }

  openDocument(attachment: Attchatment){
    let archiveUrl = this.returnBase64(attachment);
    
    fetch(archiveUrl,
    {
      method: "GET"
    }).then(res => res.blob()).then(blob => {
      this.file.writeFile(this.file.externalApplicationStorageDirectory, attachment.filename, blob, { replace: true })
      .then(res => {
        this.fileOpener.open(
          res.toInternalURL(),
          this.aplicationType
        ).then((res) => {

        }).catch(err => {
          console.log('open error')
        });
      }).catch(err => {
            console.log('save error')     
      });
      }).catch(err => {
        this.presentToast("Desculpe! Não é possível abrir este tipo de arquivo pelo celular.");
      });
  }

  returnBase64(attachment: Attchatment): string{
    let archive: string;
    let mimetype = attachment.filename.slice((attachment.filename.length - 4), attachment.filename.length);
    console.log(mimetype);

    if(mimetype === '.PDF' || mimetype === '.pdf'){
      archive = "data:application/pdf;base64," + attachment.image;    
      this.aplicationType = 'application/pdf';
    }else if(mimetype === '.JPG' || mimetype === '.jpg'){
      archive = "data:image/jpg;base64," + attachment.image;
      this.aplicationType = 'image/jpeg';
    }else if(mimetype === '.PNG' || mimetype === '.png'){
      archive = "data:image/png;base64," + attachment.image;
      this.aplicationType = 'image/png';
    }
    return archive;
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Buscando Anexos...'
    });

    loading.present();

    return loading;
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
}
