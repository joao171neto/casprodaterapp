import { Component, Input } from '@angular/core';
import { AlertController, LoadingController, Loading, ModalController } from 'ionic-angular';

import { Solicitation } from '../../models/solicitation.model';

@Component({
  selector: 'detail-solicitation',
  templateUrl: 'detail-solicitation.html'
})
export class DetailSolicitationComponent {
  @Input() solicitation: Solicitation;

  constructor(private alertController: AlertController) {}
}
