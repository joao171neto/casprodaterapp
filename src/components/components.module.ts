import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { AllSolicitationsComponent } from './all-solicitations/all-solicitations';
import { DetailSolicitationComponent } from './detail-solicitation/detail-solicitation';
import { AttachmentsComponent } from './attachments/attachments';
import { CommentsComponent } from './comments/comments';

@NgModule({
	declarations: [
		AllSolicitationsComponent,
		DetailSolicitationComponent,
    	AttachmentsComponent,
		CommentsComponent
	],
	imports: [IonicModule, HttpClientModule],
	exports: [
		AllSolicitationsComponent,
    	DetailSolicitationComponent,
		AttachmentsComponent,
    	CommentsComponent
	]
})
export class ComponentsModule {}
