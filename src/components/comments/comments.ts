import { Component, Input } from '@angular/core';
import { Solicitation } from '../../models/solicitation.model';
import { User } from '../../models/user.model';
import { Loading, Refresher, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { PreferencesService } from '../../providers/preferences.service';
import { Comment } from '../../models/comment.model';

@Component({
  selector: 'comments',
  templateUrl: 'comments.html'
})
export class CommentsComponent {
  @Input() solicitation: Solicitation;
  user: User;
  loading: Loading;
  refresher: Refresher;
  comments: Comment[] = [];
  res: Array<any>;

  constructor(
    public loadingCtrl: LoadingController,
    public preferences: PreferencesService,
    public http: HttpClient
  ) {
    this.loading = this.showLoading();
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getComments({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        }, this.solicitation)
      })
  }

  getComments(user: { username: string, password: string, id: string, org_id: string }, solicitation: Solicitation){
    this.http.get("http://cas.prodater.teresina.pi.gov.br/webservices/cas/comment/read.php?id=" + solicitation.id + "&username=" + user.username + "")
    .subscribe(data => {
      console.log(data);
      if(this.refresher){
        this.refresher.complete();
      }
      if(this.loading){
        this.loading.dismiss();
      }
    
      this.comments = [];

      for(let i = 0; i < data['time'].length; i++){
        var date_string = ("'" + data['time'][i] + "'").trim();
        var dateBr = new Date(date_string.substr(0, date_string.indexOf(' :')));
        var senderName = date_string.substr(date_string.indexOf(' :') + 2, date_string.length);
        let comm: Comment = {
          time: dateBr.toLocaleString() + " : " + senderName.substr(0, senderName.indexOf('(')).trim(),
          comment: data['comment'][i]
        }
        this.comments.unshift(comm)
      }
    })
  }

    doRefresh(refresher) {
      this.refresher = refresher;
      this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getComments({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        }, this.solicitation);
      })
    }
    formattedComment(time: string){
      let date = time.slice(0, 9);
      console.log(date);
    }
  
    private showLoading(): Loading{
      let loading: Loading = this.loadingCtrl.create({
        content: 'Buscando Comentários...'
      });
  
      loading.present();
  
      return loading;
    }
}
