import { Component } from '@angular/core';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { AlertController } from 'ionic-angular';
import { Platform, LoadingController, Loading } from 'ionic-angular';

import { Solicitation } from '../../models/solicitation.model';
import { PreferencesService } from '../../providers/preferences.service';
import { ServiceSubService } from '../../providers/service-sub.service';
import { CreateServiceService } from '../../providers/create-service.service';
import { User } from '../../models/user.model';
import { Organization } from '../../models/organization.model';
import { DetailSolicitationPage } from '../../pages/detail-solicitation/detail-solicitation';
import { SigninPage } from '../../pages/signin/signin';

import $ from 'jquery';

@Component({
  selector: 'all-solicitations',
  templateUrl: 'all-solicitations.html'
})
export class AllSolicitationsComponent {
  all_solicitations: Solicitation[] = []; //TODAS AS SOLICITAÇÕES
  showSearchBar: boolean = false;
  view: string = 'Novas';
  user: User;
  loading: Loading;
  organizations: Organization[] = []; 
  departament_selected: string = '';

  constructor(
    public navCtrl: NavController,
    public preferences: PreferencesService,
    public loadingCtrl: LoadingController,
    public createService: CreateServiceService,
    public serviceSubService: ServiceSubService,
    public platform: Platform,
    public alertCtrl: AlertController
  ) {
    this.loading = this.showLoading();
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getSolicitations({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        });
      })
  }

  doRefresh(refresher) {
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getSolicitations({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        });
      })

    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  onClick(): void {
    this.navCtrl.push(SigninPage)
  }

  getSolicitations(user: { username: string, password: string, id: string, org_id: string }): void {
    console.log(user)
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
      "operation": "core/get",
      "class": "UserRequest",
      "output_fields": "ref,org_id,org_name,caller_id,caller_name,title,description,start_date,end_date,last_update,close_date,status,request_type,origin,approver_id,approver_email, service_id,service_name,servicesubcategory_id,servicesubcategory_name,caller_id_friendlyname",
      "key": "SELECT UserRequest WHERE org_id = \'" + user.org_id +  "\'"
    }

    let data = {
      auth_user: user.username,
      auth_pwd: user.password,
      json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
      type: "POST",
      url: url,
      dataType: 'json',
      data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
      success: function (data) {
        self.all_solicitations = [];

        for (var i in data['objects']) {
          let solicitation: Solicitation = {
            id: data['objects'][i].key,
            ref: data['objects'][i].fields.ref,
            approver_email: data['objects'][i].fields.approver_email,
            approver_id: data['objects'][i].fields.approver_id,
            caller_id: data['objects'][i].fields.caller_id,
            caller_id_friendlyname: data['objects'][i].fields.caller_id_friendlyname,
            caller_name: data['objects'][i].fields.caller_name,
            close_date: data['objects'][i].fields.close_date,
            description: data['objects'][i].fields.description,
            end_date: data['objects'][i].fields.end_date,
            last_update: data['objects'][i].fields.last_update,
            org_id: data['objects'][i].fields.org_id,
            org_name: data['objects'][i].fields.org_name,
            origin: data['objects'][i].fields.origin,
            request_type: data['objects'][i].fields.request_type,
            service_id: data['objects'][i].fields.service_id,
            service_name: data['objects'][i].fields.service_name,
            servicesubcategory_id: data['objects'][i].fields.servicesubcategory_id,
            servicesubcategory_name: data['objects'][i].fields.servicesubcategory_name,
            start_date: data['objects'][i].fields.start_date,
            status: data['objects'][i].fields.status,
            title: data['objects'][i].fields.title
          }
          self.all_solicitations.unshift(solicitation);
        }

        self.loading.dismiss();
      }
    });
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Carregando Solicitações...'
    });

    loading.present();

    return loading;
  }

  onDetail(solicitation: Solicitation): void {
    this.navCtrl.push(DetailSolicitationPage, {
      solicitation: solicitation
    })
  }

  /*getOrganizations(user: { username: string, password: string }): void {
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
      "operation": "core/get",
      "class": "Organization",
      "output_fields": "name",
      "key": "SELECT Organization"
    }

    let data = {
      auth_user: user.username,
      auth_pwd: user.password,
      json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
      type: "POST",
      url: url,
      dataType: 'json',
      data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
      success: function (data) {
        self.organizations = [];

        for (var i in data['objects']) {
          let organization: Organization = {
            id: data['objects'][i].key,
            name: data['objects'][i].fields.name,
          }
          self.organizations.unshift(organization);
        }
        console.log(self.organizations)

        self.loading.dismiss();
      }
    });
  }

  showRadio() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Organizações');

    for (let i = 0; i < this.organizations.length; i++){
      let name = this.organizations[i]['name'].toLowerCase();
      alert.addInput({
        type: 'radio',
        label: name.replace(/\b\w/g, function(l){ return l.toUpperCase() }),
        value: this.organizations[i]['id']
      });
    }

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        let self = this;
        this.organizations.forEach(function(item){
          if (item.id === data){
            self.departament_selected = item.name;
          }
        });
        this.loading = this.showLoading();
        this.preferences.get()
        .then((user: User) => {
          this.user = user;
          this.getSolicitations({
            username: user.auth_user,
            password: user.auth_pwd,
            id: user.id,
            org_id: data
          });
        })
      }
    });
    alert.present();
  }*/
}
