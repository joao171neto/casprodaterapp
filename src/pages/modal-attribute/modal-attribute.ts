import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ViewController } from 'ionic-angular';
import {AlertController} from 'ionic-angular';

import { User } from '../../models/user.model';
import { Solicitation } from '../../models/solicitation.model';
import $ from 'jquery';
import { PreferencesService } from '../../providers/preferences.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Team } from '../../models/team.model';

@Component({
  selector: 'page-modal-attribute',
  templateUrl: 'modal-attribute.html',
})
export class ModalAttributePage {

  public user: User;
  public solicitation: Solicitation;
  public departaments: Team[] = [];
  public operators: User[] = [];
  attrForm: FormGroup;
  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingController: LoadingController,
    public formBuilder: FormBuilder,
    public alertController: AlertController,
    public viewCtrl: ViewController
  ) {
    this.user = this.navParams.get('user');
    this.solicitation = this.navParams.get('solicitation');

    this.attrForm = this.formBuilder.group({
        selected_departament: ['', [ Validators.required ]],
        selected_operator: ['', [ Validators.required ]]
    });

    this.getTeams();
  }

  onSelectChange(selectedValue: any): void{
    this.getOperators(selectedValue);
  }

  onSubmit(): void {
    let formAttr = this.attrForm.value;

    let alert = this.alertController.create({
      title: "Confirmar atribuição",
      subTitle: "Confirma atribuição do chamado " + this.solicitation.ref + " para o operador " + formAttr.selected_operator,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Atribuir',
          handler: () => {
            this.attribute(formAttr);
          }
        }
      ]
    });

    alert.present();
  }

  getTeams() {
    this.loading = this.showLoading('Buscando Departamentos...');
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "Contact",
        "key": "SELECT Team, Person FROM Team JOIN lnkPersonToTeam AS l ON l.team_id = Team.id JOIN Person ON l.person_id = Person.id"
    }
    
    let data = {
        auth_user: this.user.auth_user, 
        auth_pwd: this.user.auth_pwd, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) { 
          console.log(data);
            if(data.code === 0){
              for (var i in data['objects']) {
                let team: Team = {
                  _id: data['objects'][i].key,
                  name: data['objects'][i].fields.name,
                  status: data['objects'][i].fields.status,
                  org_id: data['objects'][i].fields.org_id,
                  org_name: data['objects'][i].fields.org_name,
                  phone: data['objects'][i].fields.phone,
                  notify: data['objects'][i].fields.notify,
                  function: data['objects'][i].fields.function,
                  friendlyname: data['objects'][i].fields.friendlyname,
                  org_id_friendlyname: data['objects'][i].fields.org_id_friendlyname
                }

                self.departaments.unshift(team);
              }

              self.loading.dismissAll();
            }else{
              
            }
        }
    });
  }

  getOperators(selectedValue) {
    this.loading = this.showLoading('Buscando Operadores...');
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "Contact",
        "key": "SELECT Person FROM Person JOIN lnkPersonToTeam AS l ON l.person_id = Person.id WHERE l.team_id = '" + selectedValue + "'"
    }
    
    let data = {
        auth_user: this.user.auth_user, 
        auth_pwd: this.user.auth_pwd, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) { 
          console.log(data);
            if(data.code === 0){
              for (var i in data['objects']) {
                let person: string;
                for (var i in data['objects']) {
                  person = i;
                  let user: User = {
                    id: person.substr((person.lastIndexOf('::') + 2), person.length ),
                    auth_user: '',
                    auth_pwd: '',
                    email: data['objects'][person].fields.email,
                    employee_number: data['objects'][person].fields.employee_number,
                    finalclass: data['objects'][person].fields.finalclass,
                    first_name: data['objects'][person].fields.first_name,
                    friendlyname: data['objects'][person].fields.friendlyname,
                    _function: data['objects'][person].fields.function,
                    location_id: data['objects'][person].fields.location_id,
                    location_id_friendlyname: data['objects'][person].fields.location_id_friendlyname,
                    location_name: data['objects'][person].fields.location_name,
                    manager_id: data['objects'][person].fields.manager_id,
                    manager_id_friendlyname: data['objects'][person].fields.manager_id_friendlyname,
                    manager_name: data['objects'][person].fields.manager_name,
                    mobile_phone: data['objects'][person].fields.mobile_phone,
                    name: data['objects'][person].fields.name,
                    notify: data['objects'][person].fields.notify,
                    org_id: data['objects'][person].fields.org_id,
                    org_id_friendlyname: data['objects'][person].fields.org_id_friendlyname,
                    org_name: data['objects'][person].fields.org_name,
                    phone: data['objects'][person].fields.phone,
                    status: data['objects'][person].fields.status
                  }
  
                  self.operators.unshift(user);
                }
              }

              self.loading.dismissAll();
              console.log(self.departaments);
            }else{
              
            }
        }
    });
  }

  attribute(form) {
    this.loading = this.showLoading('Atribuindo Chamado...');
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/update",
        "comment": this.user.auth_user,
        "class": "UserRequest",
        "key": this.solicitation.id,
        "stimulus": "ev_assign",
        "output_fields": "*",
        "fields":
        {
          "team_id": form.selected_departament,
          "agent_id": form.selected_operator,
          "status": "assigned"
        }
    }
    
    let data = {
        auth_user: this.user.auth_user, 
        auth_pwd: this.user.auth_pwd, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) { 
          self.loading.dismissAll();
          if(data.code === 0){
            self.viewCtrl.dismiss();
          }else{
            
          }
        }
    });
  }

  private showLoading(title:string): Loading{
    let loading = this.loadingController.create({
      content: title
    });

    loading.present();

    return loading;
  }

  private dismiss() {
    this.viewCtrl.dismiss();
  }
}
