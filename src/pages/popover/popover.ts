import { Component } from '@angular/core';

import { NavController } from 'ionic-angular/navigation/nav-controller';
import { ProfilePage } from '../profile/profile';


@Component({
  templateUrl: './popover.html'
})
export class PopoverPage {

  constructor(public navCtrl: NavController) {

  }

  onProfile(){
    this.navCtrl.push(ProfilePage);
  }
}

