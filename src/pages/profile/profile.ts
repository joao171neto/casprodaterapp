import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { User } from '../../models/user.model';
import { PreferencesService } from '../../providers/preferences.service';
import { SigninPage } from '../signin/signin';

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  user: User;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public preferences: PreferencesService
  ) {
  }

  ionViewDidLoad() {
    this.preferences.get()
    .then((user: User) => {
      this.user = user;
      console.log(this.user);
    })
  }

  logout(): void{
    this.preferences.delete();
    this.navCtrl.setRoot(SigninPage);
  }
}
