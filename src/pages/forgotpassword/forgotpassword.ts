import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import $ from 'jquery';

@Component({
  selector: 'page-forgotpassword',
  templateUrl: 'forgotpassword.html',
})
export class ForgotpasswordPage {

  @ViewChild('authuser') authuser;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              public loadingCtrl: LoadingController,
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotpasswordPage');
  }

  forgot(){
    let loading = this.showLoading();
    let self = this;
    var formData = new FormData();
    formData.append("auth_user", this.authuser.value);
 
    $.ajax({
      "method": "POST",
      "url": "http://cas.prodater.teresina.pi.gov.br/pages/UI.php?loginop=forgot_pwd_go",
      "data": formData,
      "processData": false,
      "contentType": false,
      success: (result) => {
        loading.dismiss();     
        if (result.search('Por favor, verifique seu email e siga as instruções...') >= 0) {
          this.navCtrl.setRoot(SigninPage);
          let toast = this.toastCtrl.create({
            message: 'Por favor, verifique seu email e siga as instruções...',
            duration: 3000,
            position: 'bottom'
          });
          toast.present();
        } else {
          let alert = this.alertCtrl.create({
            title: 'Erro!',
            subTitle: `Falha para enviar email: ${this.authuser.value} não é um login válido`,
            buttons: ['Ok']
          });
          alert.present();
        }         
        
      }
    })
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde um momento...'
    });

    loading.present();

    return loading;
  }
}
