import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, Loading, LoadingController } from 'ionic-angular';
import { Attchatment } from '../../models/attachment.model';

import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { PreferencesService } from '../../providers/preferences.service';
import { User } from '../../models/user.model';
import { Solicitation } from '../../models/solicitation.model';
import $ from "jquery";

@Component({
  selector: 'page-modal-content',
  templateUrl: 'modal-content.html',
})
export class ModalContentPage {
  user: any;
  aplicationType: string;
  loading: Loading;
  attachments: Attchatment[] = [];
  solicitation: Solicitation;
  
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public viewCtrl: ViewController,
    private file: File,
    private fileOpener: FileOpener,
    public preferences: PreferencesService,
    public loadingCtrl: LoadingController
  ) {
    this.solicitation = this.navParams.get('solicitation');
    console.log(this.solicitation);
  }

  ionViewDidLoad() {
    this.loading = this.showLoading();
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getAttachments({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        })
      })
  }

  getAttachments(user: { username: string, password: string, id: string, org_id: string }){
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "Attachment",
        "output_fields": "*",
        "key": "SELECT Attachment WHERE item_id LIKE '%"+ this.solicitation.id +"'"
        //"key": {"item_id":"9179"}
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
          console.log(data);

          for (var i in data['objects']) {
            let attachment: Attchatment = {
              id: data['objects'][i].key,
              image: "data:image/*;charset=utf-8;" + data['objects'][i].fields.contents.data,
              filename: data['objects'][i].fields.contents.filename,
              mimeType: data['objects'][i].fields.contents.mimeType
            }
            console.log(attachment);
            self.attachments.unshift(attachment);
          }

          self.loading.dismiss();
        }
    });
  }

  openDocument(attachment: Attchatment){
    let archiveUrl = this.returnBase64(attachment);
    
    fetch(archiveUrl,
    {
      method: "GET"
    }).then(res => res.blob()).then(blob => {
      this.file.writeFile(this.file.externalApplicationStorageDirectory, attachment.filename, blob, { replace: true })
      .then(res => {
        this.fileOpener.open(
          res.toInternalURL(),
          this.aplicationType
        ).then((res) => {

        }).catch(err => {
          console.log('open error')
        });
      }).catch(err => {
            console.log('save error')     
      });
      }).catch(err => {
           console.log('error')
      });
  }

  returnBase64(attachment: Attchatment): string{
    let archive: string;
    let mimetype = attachment.filename.slice((attachment.filename.length - 4), attachment.filename.length);
    console.log(mimetype);

    if(mimetype === '.PDF' || mimetype === '.pdf'){
      archive = attachment.image.replace("data:image/*;charset=utf-8;"
      , "data:application/pdf;base64,");

      this.aplicationType = 'application/pdf';
    }else if(mimetype === '.JPG' || mimetype === '.jpg'){
      archive = attachment.image.replace("data:image/*;charset=utf-8;"
      , "data:image/jpeg;base64,");

      this.aplicationType = 'image/jpeg';
    }else if(mimetype === '.PNG' || mimetype === '.png'){
      archive = attachment.image.replace("data:image/*;charset=utf-8;"
      , "data:image/png;base64,");

      this.aplicationType = 'image/png';
    }

    return archive;
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Buscando Anexos...'
    });

    loading.present();

    return loading;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
