import { Component } from '@angular/core';
import { 
  IonicPage, 
  NavController, 
  NavParams, 
  ViewController,
  AlertController, 
  LoadingController} from 'ionic-angular';
import { ModalAttributePage } from '../modal-attribute/modal-attribute';

import $ from 'jquery';
import { PreferencesService } from '../../providers/preferences.service';
import { User } from '../../models/user.model';
import { CurrentSolicitationService } from '../../providers/current_solicitation.service';

@Component({
  selector: 'page-popover-options-detail-solicitation',
  templateUrl: 'popover-options-detail-solicitation.html',
})
export class PopoverOptionsDetailSolicitationPage {
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private alertCtrl: AlertController,
    private preferences: PreferencesService,
    private loading: LoadingController,
    private currentSolicitationService: CurrentSolicitationService
  ) {
  }

  showAtributteModal() {
    this.viewCtrl.dismiss('showAtributteModal');
  }

  showAlertStatus() {
    this.viewCtrl.dismiss('showAlertStatus');

    //'approved','assigned','closed','escalated_tto','escalated_ttr','new','pending','rejected','resolved','waiting_for_approval'
    let alert = this.alertCtrl.create();
    alert.setSubTitle("Selecione um novo status:");
    alert.addInput({type: 'radio', label: 'Fechado', value: 'closed', checked: true});
    alert.addInput({type: 'radio', label: 'Pendente', value: 'pending', checked: false});
    alert.addInput({type: 'radio', label: 'Resolvido', value: 'resolved', checked: false});
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.preferences.get()
        .then((user: User) => {
          alert.dismiss();
          this.alterStatus(data, user);
        })
      }
    });
    alert.present();
  }

  alterStatus(status: string, user: User) {
    let loading = this.loading.create({
      content: 'Alterando status'
    });
    loading.present();

    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/update",
        "comment": user.auth_user,
        "class": "UserRequest",
        "key": this.currentSolicitationService.solicitation.id,
        "stimulus": "ev_assign",
        "output_fields": "*",
        "fields":
        {
          "status": status
        }
    }

    let data = {
        auth_user: user.auth_user, 
        auth_pwd: user.auth_pwd, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) { 
          if(data.code === 0){
            loading.dismiss();
          }else{
            
          }
        }
    });
  }
}