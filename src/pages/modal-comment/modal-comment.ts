import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, Loading, Refresher } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { Solicitation } from '../../models/solicitation.model';
import { PreferencesService } from '../../providers/preferences.service';
import { User } from '../../models/user.model';
import $ from "jquery";
import { Comment } from '../../models/comment.model';

@Component({
  selector: 'page-modal-comment',
  templateUrl: 'modal-comment.html',
})
export class ModalCommentPage {
  form: FormGroup;
  solicitation: Solicitation;
  user: User;
  loading: Loading;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public preferences: PreferencesService,
    public formBuilder: FormBuilder,
    public http: HttpClient
  ) {
    this.solicitation = this.navParams.get('solicitation');

    this.form = this.formBuilder.group({
      comment: ['', [ Validators.required, Validators.minLength(3) ]]
    });
  }
  
  onSubmit(){
    let form = this.form.value;

    this.loading = this.showLoading();
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.sendComment({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        }, this.solicitation, form.comment)
      })
  }

  sendComment(user: { username: string, password: string, id: string, org_id: string }, solicitation: Solicitation, comment: string){
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/update",
        "comment": "Update",
        "class": "UserRequest",
        "output_fields": "ref,org_id,org_name,caller_id,caller_name,title,description,start_date,end_date,last_update,close_date,status,request_type,origin,approver_id,approver_email, service_id,service_name,servicesubcategory_id,servicesubcategory_name,caller_id_friendlyname, public_log",
        "key": {
          "ref": solicitation.ref
        },
        "fields": {
          "public_log": comment
        }
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
          console.log(data);
          self.loading.dismiss();
          self.form.reset();
          //self.doRefresh(self.refresher);
        }
    });
  }

  formattedComment(time: string){
    let date = time.slice(0, 9);
    console.log(date);
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Buscando Comentários...'
    });

    loading.present();

    return loading;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
