import { Component } from '@angular/core';
import { 
  NavController, 
  NavParams, 
  Loading, 
  LoadingController, 
  ModalController,
  PopoverController} from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Solicitation } from '../../models/solicitation.model';
import { PreferencesService } from '../../providers/preferences.service';
import { User } from '../../models/user.model';
import { Attchatment } from '../../models/attachment.model';

import $ from "jquery";
import { ModalContentPage } from '../modal-content/modal-content';
import { ModalCommentPage } from '../modal-comment/modal-comment';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { PopoverOptionsDetailSolicitationPage } from '../popover-options-detail-solicitation/popover-options-detail-solicitation';
import { ModalAttributePage } from '../modal-attribute/modal-attribute';
import { CurrentSolicitationService } from '../../providers/current_solicitation.service';

@Component({
  selector: 'page-detail-solicitation',
  templateUrl: 'detail-solicitation.html',
})
export class DetailSolicitationPage {
  solicitation: Solicitation;
  user: User;
  loading: Loading;
  view: string = 'Solicitacao';

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private preferences: PreferencesService,
    private currentSolicitationService: CurrentSolicitationService
  ) {
    this.solicitation = this.navParams.get('solicitation');
    this.currentSolicitationService.setSolicitation(this.navParams.get('solicitation'));
  }

  openCommentModal(){
    let modal = this.modalCtrl.create(ModalCommentPage, { solicitation: this.solicitation });
    modal.present();
  }

  openAttributeModal() {    
    this.preferences.get()
    .then((user: User) => {
      this.user = user;
      let attrModal = this.modalCtrl.create(ModalAttributePage, {user: this.user, solicitation: this.solicitation});
      attrModal.present();
    })
  }

  presentPopover(ev) {
    let popover = this.popoverCtrl.create(PopoverOptionsDetailSolicitationPage);
    popover.present({
      ev: ev
    });

    popover.onDidDismiss((data) => {
      switch(data) {
        case 'showAtributteModal': 
          this.openAttributeModal();
          break;
        case 'shared':
          
          break;
      }
    })
  }
}
