import { Injectable } from '@angular/core';

import { User } from '../models/user.model';

import $ from "jquery";
import { PreferencesService } from './preferences.service';
import { NavController, Loading, AlertController } from 'ionic-angular';
import { HomePage } from '../pages/home/home';

@Injectable()
export class AuthService {
  public response_user: User;
  public headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8' });  

  constructor(public preferences: PreferencesService) {
    
  }

  createAuthUser(
      user: { 
          username: string, 
          password: string 
        }, 
        navCtrl: NavController, 
        loading: Loading,
        alertCtrl: AlertController
    ): void{
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "Person",
        "key": "SELECT Person AS p JOIN UserLocal AS u ON u.contactid = p.id  WHERE u.login = '" + user.username + "'"
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {          
            if(data.code === 0){
                let person: string;
                for (var i in data['objects']) {
                    person = i;
                 }
                 
                 this.response_user = {
                    id: person.substr((person.lastIndexOf('::') + 2), person.length ),
                    auth_user: user.username,
                    auth_pwd: user.password,
                    email: data['objects'][person].fields.email,
                    employee_number: data['objects'][person].fields.employee_number,
                    finalclass: data['objects'][person].fields.finalclass,
                    first_name: data['objects'][person].fields.first_name,
                    friendlyname: data['objects'][person].fields.friendlyname,
                    _function: data['objects'][person].fields.function,
                    location_id: data['objects'][person].fields.location_id,
                    location_id_friendlyname: data['objects'][person].fields.location_id_friendlyname,
                    location_name: data['objects'][person].fields.location_name,
                    manager_id: data['objects'][person].fields.manager_id,
                    manager_id_friendlyname: data['objects'][person].fields.manager_id_friendlyname,
                    manager_name: data['objects'][person].fields.manager_name,
                    mobile_phone: data['objects'][person].fields.mobile_phone,
                    name: data['objects'][person].fields.name,
                    notify: data['objects'][person].fields.notify,
                    org_id: data['objects'][person].fields.org_id,
                    org_id_friendlyname: data['objects'][person].fields.org_id_friendlyname,
                    org_name: data['objects'][person].fields.org_name,
                    phone: data['objects'][person].fields.phone,
                    status: data['objects'][person].fields.status
                }

                self.preferences.create(this.response_user)
                .then((savedUser: User) => {
                  loading.dismiss();
                  navCtrl.setRoot(HomePage);
                });
            }else{
                console.log("Erro");
                loading.dismiss();
                alertCtrl.create({
                    message: "Credenciais Inválidas. Tente Novamente!",
                    buttons: ['Ok']
                }).present();
            }
        }
    });
  }

  get authenticated(): Promise<boolean>{
    return new Promise((resolve, reject) => {
      this.preferences.get()
        .then((user: User) => {
            (user) ? resolve(true) : reject(false);
        })
    });
  }
} 