import { Injectable } from '@angular/core';
import { Solicitation } from '../models/solicitation.model';

@Injectable()
export class CurrentSolicitationService {
  public solicitation: Solicitation;

  constructor() {
    
  }

  public setSolicitation(solicitation: Solicitation) {
    this.solicitation = solicitation;
  }
} 